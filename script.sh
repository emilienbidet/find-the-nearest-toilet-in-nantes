#!/bin/bash
posLat="$(echo $1 | cut -d, -f 1)"
posLng="$(echo $1 | cut -d, -f 2)"
wget -q -o /dev/null -O toilettes.csv https://data.nantesmetropole.fr/explore/dataset/244400404_toilettes-publiques-nantes-metropole/download/?format=csv&timezone=Europe/Berlin&use_labels_for_header=true&csv_separator=%3B
sleep 1
sed -i '1d' toilettes.csv
minDif=1000
minLat=0
minLng=0
for ligne in $(awk --field-separator=";" '{print $(NF)}' toilettes.csv); do
	curLat="$(echo $ligne | cut -d, -f 1)"
	curLng="$(echo $ligne | cut -d, -f 2 | sed 's/\r//g')"
	difLat="$(echo "sqrt(($posLat - $curLat) ^ 2)" | bc)"
	difLng="$(echo "sqrt(($posLng - $curLng) ^ 2)" | bc)"
	totDif="$(echo "$difLat + $difLng" | bc)"
	if [ "$(echo "$minDif > $totDif" | bc)" -eq 1 ]; then
		minLat="$curLat"
		minLng="$curLng"
		minDif="$totDif"
	fi
done
grep "$minLat,$minLng" toilettes.csv | echo "Site        : $(awk --field-separator=";" '{print $2}')"
grep "$minLat,$minLng" toilettes.csv | echo "Adresse     : $(awk --field-separator=";" '{print $3}')"
grep "$minLat,$minLng" toilettes.csv | echo "Commune     : $(awk --field-separator=";" '{print $4}')"
grep "$minLat,$minLng" toilettes.csv | echo "Coordonneés : $(awk --field-separator=";" '{print $NF}')"
rm toilettes.csv